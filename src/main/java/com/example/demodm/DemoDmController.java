package com.example.demodm;

import com.example.demodm.jasper.JRDMDataSource;
import com.itextpdf.barcodes.BarcodeDataMatrix;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.avalon.framework.configuration.DefaultConfiguration;
import org.krysalis.barcode4j.BarcodeException;
import org.krysalis.barcode4j.BarcodeGenerator;
import org.krysalis.barcode4j.BarcodeUtil;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import uk.org.okapibarcode.backend.DataMatrix;
import uk.org.okapibarcode.backend.HumanReadableLocation;
import uk.org.okapibarcode.output.Java2DRenderer;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import static com.itextpdf.barcodes.BarcodeDataMatrix.DM_EXTENSION;
import static com.itextpdf.barcodes.BarcodeDataMatrix.DM_TEXT;
import static org.krysalis.barcode4j.impl.datamatrix.DataMatrixConstants.FNC1;

public class DemoDmController implements Initializable {
    public ImageView imageView;
    public TextField gtinFiled;
    public TextField serialField;
    public TextField checkField;

    private static String GS = "\u001D";


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void onBarcode4jButtonClick(ActionEvent actionEvent) throws ConfigurationException, BarcodeException, IOException {
        //создаем генератор штрихкода
        BarcodeUtil util = BarcodeUtil.getInstance();
        BarcodeGenerator gen = util.createBarcodeGenerator(new DefaultConfiguration("datamatrix"));

        BitmapCanvasProvider canvas = new BitmapCanvasProvider(
                new ByteArrayOutputStream (), "image/jpeg", 1200, BufferedImage.TYPE_BYTE_BINARY, false, 0);

        //заполняем данные и генерируем код
        gen.generateBarcode(canvas, FNC1+getCodeBarcode4j());
        canvas.finish();

        //отрисовываем картинку
        Image image = SwingFXUtils.toFXImage(canvas.getBufferedImage(), null);
        imageView.setImage(image);
    }

    public void onOkapibarcodeButtonClick(ActionEvent actionEvent) {
        //создаем генератор штрихкода
        DataMatrix barcode = new DataMatrix();

        barcode.setFontName("Monospaced");
        barcode.setFontSize(20);
        barcode.setModuleWidth(2);
        barcode.setBarHeight(50);

        barcode.setHumanReadableLocation(HumanReadableLocation.BOTTOM);
        barcode.setDataType(uk.org.okapibarcode.backend.Symbol.DataType.GS1);
        barcode.setGs1SeparatorGs(true);
        //заполняем данные
        barcode.setContent(getCodeOkapibarcode());

        //генерируем код
        int width = barcode.getWidth();
        int height = barcode.getHeight();

        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
        Graphics2D g2d = bufferedImage.createGraphics();
        Java2DRenderer renderer = new Java2DRenderer(g2d, 1, Color.WHITE, Color.BLACK);
        renderer.render(barcode);

        //отрисовываем картинку
        Image image = SwingFXUtils.toFXImage(bufferedImage, null);
        imageView.setImage(image);
    }

    public void onItextButtonClick(ActionEvent actionEvent) {
        //создаем генератор штрихкода
        BarcodeDataMatrix dataMatrix = new BarcodeDataMatrix();
        dataMatrix.setOptions(DM_EXTENSION + DM_TEXT);

        //заполняем данные
        dataMatrix.setCode("f." + getCodeBarcode4j());

        //генерируем код
        var awtImage = dataMatrix.createAwtImage(Color.BLACK, Color.WHITE);

        //отрисовываем картинку
        Image image = SwingFXUtils.toFXImage(awtImageToBufferedImage(awtImage), null);
        imageView.setImage(image);
    }


    public void onJasperReportButtonAction(ActionEvent actionEvent) throws JRException {
        JRDataSource dataSource = new JRDMDataSource(List.of(getCodeBarcode4j()));
        JasperReport jasperReport = getJasperTemplate();



        JasperPrint jasperPrint = JasperFillManager.fillReport(
                jasperReport, null, dataSource);


        JRPdfExporter exporter = createJRPdfExporter();

        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(
                new SimpleOutputStreamExporterOutput(getDestination()));

        exporter.exportReport();
    }

    public void onJasperReporterHackedButtonAction(ActionEvent actionEvent) throws JRException {

        JRDataSource dataSource = new JRDMDataSource(List.of(getCodeBarcode4j()));
        JasperReport jasperReport = getJasperTemplate();

        DefaultJasperReportsContext ctx = DefaultJasperReportsContext.getInstance();
        ctx.setProperty("net.sf.jasperreports.components.barcode4j.image.producer.svg", "com.example.demodm.jasper.BarcodeSVGImageProducer");

        JasperPrint jasperPrint = JasperFillManager.fillReport(
                jasperReport, null, dataSource);

        JRPdfExporter exporter = createJRPdfExporter();

        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(
                new SimpleOutputStreamExporterOutput(getDestination()));

        exporter.exportReport();

        exporter.exportReport();
    }


    private String getCodeBarcode4j() {
    //"0104640199927083215NKghuiJ=wmDm\u001D91EE06\u001D92w8LJ5JgQPxTxbbkgmL5yZsp/fNzhK0FgA9wuZT01D8s=";
        return "01" + gtinFiled.getText() + "21" + serialField.getText() + GS + "93" + checkField.getText();
    }

    private String getCodeOkapibarcode() {
    //"[01]04640199927083[21]5NKghuiJ=wmDm[91]EE06[92]w8LJ5JgQPxTxbbkgmL5yZsp/fNzhK0FgA9wuZT01D8s=";
        return "[01]" + gtinFiled.getText() + "[21]" + serialField.getText() + "[93]" + checkField.getText();
    }

    private static BufferedImage awtImageToBufferedImage(java.awt.Image awtImage) {
        BufferedImage bImage = new BufferedImage(awtImage.getWidth(null), awtImage.getHeight(null), BufferedImage.TYPE_BYTE_GRAY);
        Graphics2D g = bImage.createGraphics();
        g.drawImage(awtImage, 0, 0, null);
        g.dispose();
        return bImage;
    }


//    private static Configuration buildBarcode4JCfg(String type) {
//        DefaultConfiguration cfg = new DefaultConfiguration("barcode");
//
//        //Bar code type
//        DefaultConfiguration child = new DefaultConfiguration(type);
//        cfg.addChild(child);
//
//        //Human readable text position
//        DefaultConfiguration attr = new DefaultConfiguration("human-readable");
//        DefaultConfiguration subAttr = new DefaultConfiguration("placement");
//        subAttr.setValue("bottom");
//        attr.addChild(subAttr);
//
//        child.addChild(attr);
//        return cfg;
//    }

    private JRPdfExporter createJRPdfExporter() {
        JRPdfExporter exporter = new JRPdfExporter();
        SimplePdfReportConfiguration reportConfig
                = new SimplePdfReportConfiguration();
        reportConfig.setSizePageToContent(true);
        reportConfig.setForceLineBreakPolicy(false);

        SimplePdfExporterConfiguration exportConfig
                = new SimplePdfExporterConfiguration();
        exportConfig.setMetadataAuthor("JPoint");
        exportConfig.setEncrypted(true);
        exportConfig.setAllowedPermissionsHint("PRINTING");

        exporter.setConfiguration(reportConfig);
        exporter.setConfiguration(exportConfig);

        return exporter;
    }

    private File getDestination() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Сохранить pdf");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Codes (*.pdf)", "*.pdf"));
        fileChooser.setInitialFileName("jasper.pdf");
        Stage stage = (Stage) imageView.getScene().getWindow();
        return fileChooser.showSaveDialog(stage);

    }

    private JasperReport getJasperTemplate() throws JRException {
        return  (JasperReport) JRLoader.loadObject(new File("/Users/viktor.barsukov/IdeaProjects/demoDM/target/classes/com/example/demodm/template.jasper"));
    }
}