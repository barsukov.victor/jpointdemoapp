package com.example.demodm.jasper;

import com.itextpdf.barcodes.BarcodeDataMatrix;
import net.sf.jasperreports.components.barcode4j.BarcodeImageProducer;
import net.sf.jasperreports.engine.JRComponentElement;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.renderers.Renderable;
import net.sf.jasperreports.renderers.SimpleRenderToImageAwareDataRenderer;
import org.krysalis.barcode4j.BarcodeGenerator;
import org.krysalis.barcode4j.impl.datamatrix.DataMatrixBean;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static com.itextpdf.barcodes.BarcodeDataMatrix.*;

public class BarcodeSVGImageProducer implements BarcodeImageProducer {
    @Override
    public Renderable createImage(
            JasperReportsContext jasperReportsContext,
            JRComponentElement componentElement,
            BarcodeGenerator barcode,
            String message
    ) {
        try {

            int height = Double.valueOf(((DataMatrixBean) barcode).getHeight()).intValue();
            boolean quietZone = ((DataMatrixBean) barcode).hasQuietZone();

            BarcodeDataMatrix dataMatrix = new BarcodeDataMatrix();
            dataMatrix.setHeight(height);
            if (quietZone) {
                int qz = Double.valueOf(((DataMatrixBean) barcode).getQuietZone()).intValue();
                dataMatrix.setWs(qz);

            }
            dataMatrix.setOptions(DM_EXTENSION + DM_TEXT);
            dataMatrix.setCode("f." + message);
            dataMatrix.setOptions(DM_AUTO);
            var image = dataMatrix.createAwtImage(Color.BLACK, Color.WHITE);

            return SimpleRenderToImageAwareDataRenderer.getInstance(toByteArray(toBufferedImage(image), "png"));
        } catch (Exception e) {
            throw new JRRuntimeException(e);
        }
    }

    public static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }

    public static byte[] toByteArray(BufferedImage bi, String format)
            throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bi, format, baos);
        byte[] bytes = baos.toByteArray();
        return bytes;

    }


}
