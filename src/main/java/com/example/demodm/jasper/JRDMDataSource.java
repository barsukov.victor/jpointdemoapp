package com.example.demodm.jasper;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import java.util.Iterator;
import java.util.List;

public class JRDMDataSource  implements JRDataSource {
    private static final String DATAMATRIX_FIELD = "dataMatrix";
    private static final String CIS_FIELD = "cis";
    private Iterator<String> iterator;
    private String curElement;

    public JRDMDataSource(List<String> codes) {

        this.iterator = codes.iterator();
    }

    @Override
    public boolean next() throws JRException {
        var result = iterator.hasNext();
        if (result) {
            curElement = iterator.next();
        }
        return result;
    }

    @Override
    public Object getFieldValue(JRField jrField) throws JRException {
        if (DATAMATRIX_FIELD.equals(jrField.getName())) {
            return  curElement;
        } else if (CIS_FIELD.equals(jrField.getName())) {
            return curElement.split("\\u001D")[0];
        }
        return null;
    }

}
